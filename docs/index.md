# k-Nearest Neighbors 알고리즘과 응용 <sup>[1](#footnote_1)</sup>

> <font size="3">분류와 회귀 작업에 k-NN(k-Nearest Neighbors) 알고리즘을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./knn.md#intro)
1. [k-NN 알고리즘이란?](./knn.md#sec_02)
1. [Python에서 k-NN 알고리즘 구현 방법](./knn.md#sec_03)
1. [k의 최적값을 선택하는 방법](./knn.md#sec_04)
1. [k-NN 알고리즘의 성능 평가](./knn.md#sec_05)
1. [k-NN의 장단점](./knn.md#sec_06)
1. [k-NN의 실제 적용](./knn.md#sec_07)
1. [마치며](./knn.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 6 — k-Nearest Neighbors Algorithm and Applications](https://medium.datadriveninvestor.com/ml-tutorial-6-k-nearest-neighbors-algorithm-and-applications-655c751a1e94?sk=5aaa8b8bd905d1d4beabcb3e41f6597b)를 편역하였다.
