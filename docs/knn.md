# k-Nearest Neighbors 알고리즘과 응용

## <a name="intro"></a> 들어가며
이 포스팅에서는 다음과 같은 내용을 설명한다.

- k-NN알고리즘은 무엇이며 어떻게 작동하는가?
- 분류와 회귀 작업을 위해 Python에서 k-NN알고리즘을 어떻게 구현할까?
- 데이터세트에 대한 k의 최적 값을 어떻게 선택할까?
- k-NN 알고리즘의 성능을 평가하고 다른 기계 학습 알고리즘과 비교하는 방법은 무엇일가?
- k-NN 알고리즘의 장단점은 무엇일까?
- k-NN 알고리즘의 어떻게 실제에 적용할 수 있을까?

이 포스팅을 읽고 다음에는 k-NN 알고리즘과 이를 다양한 데이터 분석 문제에 사용하는 방법에 대해 확실하게 이해할 수 있을 것이다.

그러나 세부 사항으로 들어가기 전에 간단한 질문부터 시작하겠습니다. k-NN알고리즘은 무엇인가?

## <a name="sec_02"></a> k-NN 알고리즘이란?
k-최근접 이웃(kNN) 알고리즘은 가장 간단하고 널리 사용되는 기계 학습 알고리즘 중 하나이다. 이는 레이블이 지정된 데이터, 즉 알려진 결과 또는 대상 변수가 있는 데이터에서 학습하는 것을 의미하는 지도 학습의 한 유형이다. 대상 변수는 범주형(분류 작업의 경우) 또는 수치형(회귀 작업의 경우)일 수 있다.

kNN 알고리즘의 기본 아이디어는 새로운 데이터 포인트와 가장 유사하거나 가장 가까운 k개의 데이터 포인트를 찾고 이들의 레이블 또는 값을 사용하여 새로운 데이터 포인트의 레이블 또는 값을 예측하는 것이다. 유사성 또는 근접성은 일반적으로 유클리드 거리, 맨해튼 거리 또는 해밍(Hamming) 거리 같은 거리 메트릭에 의해 측정된다. k-NN 알고리즘은 다음과 같이 요약할 수 있다.

1. 새 데이터 포인트가 주어지면 해당 데이터 포인트와 훈련 세트의 다른 모든 데이터 포인트 사이의 거리를 계산한다.
1. 거리를 오름차순으로 정렬하고 거리가 가장 작은 k개의 데이터 포인트를 선택한다.
1. 작업이 분류이면 새 데이터 포인트에 k개의 데이터 포인트 중에서 가장 빈도가 높은 레이블을 할당하고 작업이 회귀이면 새 데이터 포인트에 k개의 데이터 포인트의 평균값을 할당한다.

k-NN 알고리즘은 학습 데이터로부터 모델을 구축하거나 매개변수를 학습하지 않기 때문에 게으른 학습자라고도 한다. 단순히 학습 데이터를 저장하고 이를 기반으로 예측을 한다. 이것은 k-NN 알고리즘을 구현하고 이해하기 매우 쉽도록 해줄 뿐만 아니라 계산 비용이 많이 들고 노이즈와 이상치에 민감하다.

다음 절에서는 분류와 회귀 작업을 위해 Python에서 k-NN 알고리즘을 구현하는 방법을 알아보겠다.

## <a name="sec_03"></a> Python에서 k-NN 알고리즘 구현 방법
이 절에서는 분류와 회귀 작업을 위해 Python에서 k-NN 알고리즘을 구현하는 방법을 알아보겠다. Python에서 머신 러닝을 위한 인기 있고 강력한 도구인 scikit-learn 라이브러리를 사용할 것이다. scikit-learn은 분류를 위한 `KNeighborsClassifier` 클래스와 회귀를 위한 `KNeighborsRegressor` 클래스를 제공하는데, 이 둘은 모두 k-NN 알고리즘을 기반으로 한다.

이러한 클래스를 사용하려면 다음 단계를 따라야 한다.

1. scikit-learn에서 필요한 모듈을 임포트한다.
1. 피쳐(X)와 레이블 또는 값(y)으로 구성된 데이터세트를 로드하거나 만든다.
1. `train_test_split` 함수를 사용하여 데이터세트를 훈련과 테스트 세트로 분할한다.
1. k 값과 거리 메트릭을 지정하여 `KNeighborsClassifier` 또는 `KNeighborsRegressor` 클래스의 인스턴스를 만든다.
1. 적합 방법을 사용하여 모형을 훈련 데이터에 적합시킨다.
1. 예측 방법을 사용하여 검정 데이터에 대한 레이블 또는 값을 예측한다.
1. 점수(score) 방법 또는 기타 메트릭을 사용하여 모델의 정확도 또는 성능을 평가한다.

scikit-learn을 사용하여 분류를 위한 k-NN 알고리즘을 구현하는 방법의 예를 살펴보겠다. 우리는 setosa, versicolor와 virginica 세 종의 iris 꽃 150개의 측정값을 포함하는 알려진 데이터세트인 iris 데이터세트를 사용한다. 특징은 sepal length, sepal width, petal length와 petal width이며, 레이블은 종이다. 우리의 목표는 측정값을 기반으로 새로운 iris 꽃의 종을 예측할 수 있는 k-NN 분류기를 구축하는 것이다.

iris 데이터 세트에 대한 kNN 분류기를 구현하기 위한 코드는 다음과 같다.

```python
# Import the necessary modules
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

# Load the iris data set
iris = load_iris()
X = iris.data # Features
y = iris.target # Labels

# Split the data set into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create an instance of the KNeighborsClassifier class with k=5 and Euclidean distance
knn = KNeighborsClassifier(n_neighbors=5, metric='euclidean')

# Fit the model to the training data
knn.fit(X_train, y_train)

# Predict the labels for the testing data
y_pred = knn.predict(X_test)

# Evaluate the accuracy of the model
accuracy = knn.score(X_test, y_test)
print("The accuracy of the kNN classifier is:", accuracy)
```

코드의 출력은 다음과 같다.

```
The accuracy of the kNN classifier is: 1.0
```

이는 k-NN 분류기가 모든 테스트 데이터의 종을 정확하게 예측했다는 것을 의미한다. 그러나 k-NN 분류기가 k의 선택, 거리 메트릭 및 데이터세트에 따라 달라질 수 있기 때문에 항상 완벽하다는 것을 의미하지 않는다. [다음 절](#sec_04)에서는 데이터세트에 대한 최적의 k 값을 선택하는 방법을 알아보겠다.

## <a name="sec_04"></a> k의 최적값을 선택하는 방법
k-NN 알고리즘의 가장 중요한 매개변수 중 하나는 예측을 위해 고려해야 할 가장 가까운 이웃(nearest neighbor)의 수인 k의 값이다. k의 최적 값을 선택하는 것은 데이터 세트의 특성과 당면한 문제에 따라 달라지기 때문에 중요하다. 일반적으로 k의 선택에 영향을 미치는 두 가지 주요 요인이 있다.

- **편향과 분산 사이의 상충 관계(trade-off between bias and variance)**: 편향은 모형의 단순화로 인한 오차이고 분산은 모형이 훈련 데이터에 민감하여 발생하는 오차이다. k 값이 작으면 모형이 더 복잡하고 유연하지만 과적합과 높은 분산을 가지기 쉽다는 것을 의미한다. k 값이 크면 모형이 더 단순하고 안정적이지만 과소적합과 높은 편향을 가지기 쉽다는 것을 의미한다. 따라서 k의 최적값은 편향과 분산의 균형을 맞추고 총 오차를 최소화하는 값이다.
- **데이터 세트의 크기와 분포**: k의 값은 데이터세트의 데이터 포인트 수보다 작아야 하며, 그렇지 않으면 모델은 항상 새로운 데이터 포인트에 대해 동일한 레이블 또는 값을 예측한다. k의 값은 데이터세트의 분포, 즉 데이터 포인트의 밀도 및 다양성과도 호환되어야 한다. 데이터세트가 희소하고 이질적인 경우 패턴을 캡처하고 노이즈를 피하기 위해 더 큰 k의 값이 필요할 수 있습니다. 데이터세트의 밀도가 높고 균질한 경우 더 작은 k의 값은 우수한 정확도를 달성하고 중복을 피하기에 충분할 수 있다.

교차 검증(cross-validation), 그리드 검색(grid search) 또는 엘보(elbow) 방법 등 최적의 k 값을 선택하는 다양한 방법과 기술이 있다. 이 포스팅에서는 서로 다른 모델과 매개 변수를 평가하고 비교하는 일반적이고 효과적인 방법인 교차 검증 방법을 사용한다. 교차 검증은 데이터세트를 k 개의 폴드(fold)로 분할하고 하나의 폴드를 테스트세트로 사용하고 나머지를 훈련 세트로 사용하여 이 프로세스를 k 번 반복하는 것이다. 그런 다음 k 개의 접기에 걸친 모델의 평균 정확도 또는 성능이 가장 좋은 k 값을 선택하는 기준으로 사용된다.

교차 검증을 사용하여 iris 데이터세트에 대한 최적의 k 값을 선택하는 방법을 알아보겠다. 각 폴드에 대한 모델의 정확도 또는 성능 점수를 반환하는 scikit-learn의 `cross_val_score` 함수를 사용할 것이다. 또한 matplotlib 라이브러리를 사용하여 점수를 k의 함수로 표시하고 k의 최적 값을 시각화한다.

교차 검증을 사용하여 iris 데이터세트에 대한 k의 최적 값을 선택하는 코드는 다음과 같다.

```python
# Import the necessary modules
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt

# Load the iris data set
iris = load_iris()
X = iris.data # Features
y = iris.target # Labels

# Create a list of possible values of k from 1 to 30
k_range = range(1, 31)

# Create an empty list to store the cross-validation scores
cv_scores = []

# Loop through the possible values of k
for k in k_range:
    # Create an instance of the KNeighborsClassifier class with k and Euclidean distance
    knn = KNeighborsClassifier(n_neighbors=k, metric='euclidean')
    # Perform 10-fold cross-validation on the data set and get the accuracy scores
    scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')
    # Append the mean of the scores to the cv_scores list
    cv_scores.append(scores.mean())

# Plot the cv_scores as a function of k
plt.plot(k_range, cv_scores)
plt.xlabel('Value of k')
plt.ylabel('Cross-validation accuracy')
plt.title('Choosing the optimal value of k for the iris data set')
plt.show()
```

코드의 출력은 다음과 같다.

![](./images/1_NLsJkNGUGdYEq8XTMl8GAQ.webp)

그림에서 볼 수 있듯이, 교차 검증 정확도는 k가 10에서 20 정도일 때 가장 높고, k가 증가함에 따라 감소한다. 따라서 iris 데이터세트에 대한 최적 k 값은 10에서 20 사이에 있다. 우리는 또한 최대 함수를 사용하여 가장 높은 정확도를 제공하는 k 값을 찾을 수 있다.

```python
# Find the value of k that gives the highest accuracy
optimal_k = k_range[cv_scores.index(max(cv_scores))]
print("The optimal value of k for the iris data set is:", optimal_k)
```

코드의 출력은 다음과 같다.

```
The optimal value of k for the iris data set is: 13
```

따라서 iris 데이터세트의 최적 k 값은 13이므로 13개의 가장 가까운 이웃을 사용하여 새로운 데이터 포인트를 대한 예측을 해야 한다. [다음 절](#sec_05)에서는 k-NN 알고리즘의 성능을 평가하고 다른 기계 학습 알고리즘과 비교하는 방법을 알아보겠다.

## <a name="sec_05"></a> k-NN 알고리즘의 성능 평가
이 절에서는 k-NN 알고리즘의 성능을 평가하고 다른 기계 학습 알고리즘과 비교하는 방법을 알아보겠다. 기계 학습 알고리즘의 성능을 평가하는 것은 주어진 문제에 대한 효율성과 적합성을 평가하는 데 필수적인 단계이다. 기계 학습 알고리즘의 성능을 측정하는 메트릭과 방법은 작업 유형과 데이터세트에 따라 다르다.

분류 작업의 경우 일반적인 메트릭 중 일부는 정확도, 정밀도, 리콜, f1-스코어, 혼동 행렬 및 ROC 곡선이다. 정확도는 전체 예측 중 올바른 예측의 비율이다. 정밀도는 긍정적인 예측 중 참 긍정의 비율이다. 리콜은 실제 긍정의 비율 중 참 긍정의 비율이다. F1-스코어는 정밀도와 리콜의 조화 평균이다. 혼동 행렬은 참 긍정, 거짓 긍정, 참 부정 및 거짓 부정의 수를 보여주는 표이다. ROC 곡선은 다른 임계값에 대한 참 긍정률과 거짓 긍정률 사이의 균형을 보여주는 그림이다.

회귀 분석 작업의 경우 일반적인 메트릭 중 일부는 평균 절대 오차, 평균 제곱 오차, 평균 제곱 오차, 결정 계수 및 잔차(residual) 그림이다. 평균 절대 오차는 예측값과 실제 값 사이의 절대적인 차이의 평균이다. 평균 제곱 오차는 예측값과 실제 값 사이의 차이 제곱의 평균이다. 평균 제곱 오차는 평균 제곱 오차의 제곱근이다. 결정 계수(coefficirnt of determination)는 실제 값에서 모델에 의해 설명되는 분산의 비율이다. 잔차 그림은 실제 값 또는 예측값의 함수로서 예측값과 실제 값 사이의 차이를 보여주는 그림이다.

scikit-learn은 `accuracy_score`, `precision_score`, `recall_score`, `f1_score`, `confusion_matrix`, `roc_curve`, `mean_absolute_error`, `mean_squared_error`, `r2_score`, `residual_plot` 등 같은 메트릭을 계산하고 표시하는 다양한 함수와 클래스를 제공한다. 이러한 함수와 클래스를 사용하여 iris 데이터세트에 대한 k-NN 알고리즘의 성능을 평가하고 로지스틱 회귀, 의사 결정 트리, 서포트 벡터 머신과 같은 다른 기계 학습 알고리즘과 비교할 수 있다.

scikit-learn을 사용하여 분류를 위한 k-NN 알고리즘의 성능을 평가하는 예를 살펴보겠다. iris 데이터세트에 대한 k-NN 분류기를 구현하기 위해 [앞 절](#sec_04)과 동일한 코드를 사용하지만 클래스별 정확도, 정밀도, 리콜 및 f1-score의 요약을 반환하는 `classification_report` 함수와 모델에 대한 혼동 행렬을 플롯하는 `plot_confusion_matrix` 함수를 임포트하여 사용한다. iris 데이터세트에 대한 k-NN 분류기의 성능을 평가하기 위한 코드는 다음과 같다.

```python
# Import the necessary modules
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, plot_confusion_matrix
import matplotlib.pyplot as plt

# Load the iris data set
iris = load_iris()
X = iris.data # Features
y = iris.target # Labels

# Split the data set into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create an instance of the KNeighborsClassifier class with k=13 and Euclidean distance
knn = KNeighborsClassifier(n_neighbors=13, metric='euclidean')

# Fit the model to the training data
knn.fit(X_train, y_train)

# Predict the labels for the testing data
y_pred = knn.predict(X_test)

# Evaluate the performance of the model using the classification_report function
report = classification_report(y_test, y_pred, target_names=iris.target_names)
print("The performance report of the kNN classifier is:\n", report)

# Plot the confusion matrix of the model using the plot_confusion_matrix function
plot_confusion_matrix(knn, X_test, y_test, display_labels=iris.target_names, cmap=plt.cm.Blues)
plt.title('The confusion matrix of the kNN classifier')
plt.show()
```

코드의 출력은 다음과 같다.

```
The performance report of the kNN classifier is:
                  precision    recall  f1-score   support

        setosa       1.00      1.00      1.00        10
    versicolor       1.00      1.00      1.00         9
     virginica       1.00      1.00      1.00        11

      accuracy                           1.00        30
     macro avg       1.00      1.00      1.00        30
  weighted avg       1.00      1.00      1.00        30
```

![](./images/screenShot_01.png)

보고서와 도표에서 볼 수 있듯이 kNN 분류기는 완벽한 정확도 1.0을 달성했으며, 이는 모든 테스트 데이터의 종을 정확하게 예측했음을 의미한다. 각 클래스의 정밀도, 리콜 및 f1-점수도 1.0이며, 이는 k-NN 분류기가 거짓 양성(false positive) 또는 거짓 음성(false negative)을 만들지 않았음을 의미한다. 혼동 행렬은 k-NN 분류기가 10개의 setosa, 9개의 versicolor 및 11개의 virginica 꽃을 올바르게 분류했음을 보여준다.

그러나 데이터 세트와 문제에 따라 달라질 수 있기 때문에 k-NN 분류기가 항상 분류에 가장 적합한 기계 학습 알고리즘이라는 것을 의미하는 것은 아니다. [다음 절](#sec_06)에서는 k-NN 알고리즘을 로지스틱 회귀, 의사 결정 트리 및 서포트 벡터 머신 같은 다른 기계 학습 알고리즘과 비교하는 방법을 알아보겠다.

## <a name="sec_06"></a> k-NN의 장단점
이 절에서는 k-NN 알고리즘의 장단점이 무엇인지 알아보겠다. k-NN 알고리즘은 단순하고 직관적인 머신러닝 알고리즘으로 분류 작업과 회귀 작업 모두에 사용할 수 있다. 하지만 다른 머신러닝 알고리즘과 마찬가지로 장단점이 있으며, 이에 대해서는 아래에서 설명하겠다.

k-NN 알고리즘의 장점은 다음과 같다.

- 복잡한 수학적 계산이나 가정이 필요하지 않기 때문에 쉽게 구현하고 이해할 수 있다.
- 수치, 범주형 또는 텍스트와 같은 모든 유형의 데이터와 함께 작업할 수 있기 때문에 유연하고 적응할 수 있다.
- 다수결 또는 이웃의 평균을 사용하여 이상치와 결측치를 처리할 수 있기 때문에 강력하고 노이즈에 강하다.
- 비모수적이다. 이는 데이터의 기본 분포나 모형의 형태에 대한 어떠한 가정도 하지 않는다는 것을 의미한다.

k-NN 알고리즘의 단점은 다음과 같다.

- 모든 데이터 포인트 쌍 간의 거리를 계산하고 전체 학습 데이터를 메모리에 저장해야 하기 때문에 계산 비용이 많이 들고 느리다.
- 이러한 매개 변수는 알고리즘의 정확도와 성능에 영향을 미칠 수 있기 때문에 k, 거리 메트릭 및 특징의 스케일 선택에 민감하다.
- 차원성의 저주(curse of dimensionality)로 고통 받는데, 이는 특징이나 차원의 수가 증가할수록 데이터 포인트 간의 거리가 의미가 없어지고 알고리즘의 효과가 떨어지게 된다는 것을 의미한다.
- 데이터 포인트의 유사성이나 근접성에 의존하기 때문에 예측의 논리나 추론에 대한 설명이나 통찰력을 제공하지 않는다.

따라서 k-NN 알고리즘은 다양한 데이터 분석 문제에 활용할 수 있는 강력하고 다재다능한 머신러닝 알고리즘이지만 고려하고 해결해야 할 몇 가지 한계와 과제도 가지고 있다. [다음 절](#sec_07)에서는 k-NN 알고리즘의 실제 적용 사례를 알아보겠다.

## <a name="sec_07"></a> k-NN의 실제 적용
k-NN 알고리즘은 다양한 실세계 문제와 영역에 적용할 수 있는 다재다능하고 강력한 머신러닝 알고리즘이다. k-NN 알고리즘을 적용한 예는 다음과 같다.

- **이미지 인식 및 분류**: k-NN 알고리즘을 사용하여 픽셀 값이나 특징을 기반으로 이미지를 인식하고 분류할 수 있다. 예를 들어, k-NN 알고리즘을 사용하여 이미지에서 손으로 쓴 숫자, 얼굴 또는 물체를 데이터베이스의 이미지와 비교하여 가장 유사한 것을 찾을 수 있다.
- **텍스트 분석 및 감정 분석**: k-NN 알고리즘을 사용하여 텍스트 문서를 내용이나 감정에 따라 분석하고 분류할 수 있다. 예를 들어, k-NN 알고리즘을 사용하여 뉴스 기사, 리뷰 또는 트윗을 말뭉치의 텍스트 문서와 비교하고 가장 유사한 것을 찾아냄으로써 서로 다른 주제나 감정으로 분류할 수 있다.
- **추천 시스템 및 개인화**: k-NN 알고리즘을 사용하여 사용자의 선호도나 행동을 기반으로 제품, 서비스 또는 콘텐츠를 사용자에게 추천할 수 있다. 예를 들어 k-NN 알고리즘을 사용하여 사용자의 평점 또는 구매액을 다른 사용자의 평점 또는 구매액과 비교하여 가장 유사한 것을 찾아냄으로써 사용자에게 영화, 책 또는 음악을 제안할 수 있다.
- **이상 탐지 및 사기 탐지**: k-NN 알고리즘을 사용하여 정상적인 패턴이나 행동에서 벗어난 데이터세트의 이상 징후나 이상치를 탐지할 수 있다. 예를 들어, k-NN 알고리즘을 사용하여 정상적인 거래, 네트워크 활동 또는 의료 기록과 비교하여 가장 유사하지 않은 상태를 찾아 부정 거래, 네트워크 침입 또는 의료 상태를 탐지할 수 있다.

이들은 k-NN 알고리즘을 응용한 사례 중 일부에 불과하지만, 이 알고리즘을 다양한 데이터 분석 문제를 해결하는 데 사용할 수 있는 가능성과 기회는 더 많다. [다음 절](#summary)에서는 이 포스팅을 마무리하고 학습한 주요 사항을 요약하겠다.

## <a name="summary"></a> 마치며
이 포스팅에서 우리는 k-NN 알고리즘과 그 응용에 대해 배웠다. 우리는 scikit-learn 라이브러리를 사용하여 분류와 회귀 작업을 위해 Python에서 k-NN 알고리즘을 구현하는 방법을 보였다. 또한 교차 검증을 사용하여 데이터세트에 대한 최적의 k 값을 선택하는 방법, 다양한 메트릭을 사용하여 k-NN 알고리즘의 성능을 평가하는 방법, k-NN 알고리즘을 다른 기계 학습 알고리즘과 비교하는 방법도 보였다. 마지막으로, 우리는 k-NN 알고리즘과 일부 실제 응용 프로그램의 장단점에 대해 논의했다.

k-NN 알고리즘은 단순하고 강력한 머신러닝 알고리즘으로 다양한 데이터 분석 문제와 영역에 적용할 수 있다. 하지만 완벽한 알고리즘은 아니며, 한계와 과제가 있다. 따라서 k-NN 알고리즘의 장단점을 파악하여 현명하고 적절하게 사용하는 것이 중요하다.
